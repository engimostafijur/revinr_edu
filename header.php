<!DOCTYPE html>

<html lang="en">

    <head>

        <meta charset="utf-8">

        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

        <title> Welcome to Eduaid</title>

        <link rel="SHORTCUT ICON" href="img/fb1.png" />

        <!-- Bootstrap -->

        <link href="css/bootstrap.css" rel="stylesheet">

      

        <link rel="stylesheet" type="text/css" href="css/demo.css" />

        <link rel="stylesheet" type="text/css" href="css/style.css" />

        <link rel="stylesheet" type="text/css" href="css/custom.css" />

        <link rel="stylesheet" href="css/bootstrap-select.css">

         <link rel="stylesheet" href="css/custom_responsive.css">

        <script type="text/javascript" src="js/modernizr.custom.79639.js"></script>

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->

        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->

        <!--[if lt IE 9]>

        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>

        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>

        <![endif]-->

        <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,300,200,700' rel='stylesheet' type='text/css'>

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

          

        </head>

      <body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">

            <!-- Static navbar -->

            <nav class="navbar navbar-default navbar-fixed-top">

                <div class="container">

                    <div class="navbar-header page-scroll">

                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">

                        <span class="sr-only">Toggle navigation</span>

                        <span class="icon-bar"></span>

                        <span class="icon-bar"></span>

                        <span class="icon-bar"></span>

                        </button>

                           <!--   <a class="navbar-brand page-scroll" href="#page-top">Start Bootstrap</a> -->

                      <a class="navbar-brand" href="index.php"><img src="img/logo.png" alt=""></a>  

                    </div>

                    <div id="navbar" class="navbar-collapse collapse">

                      <ul class="nav navbar-nav">

                        <li class="hidden"><a class="page-scroll" href="#page-top">HOME</a></li> 

                          <!--  <li><a class="page-scroll" href="#page-top">HOME</a></li> -->

                          <li class="about"><a class="page-scroll animate" href="#about">ABOUT US</a></li>

                            <!-- <li><a class="page-scroll" href="#skill">SKILLED MIGRATION</a></li> -->

                               <li class="dropdown">

                                    <a href="#" class="dropdown-toggle animate" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">

                                    VISA TYPES <span class="fa fa-caret-down rotate"></span></a>

                                        <ul class="dropdown-menu">

                                        <li><a class="page-scroll animate" href="index.php#skill">AUSTRALIA</a></li>

                                       <li><a class="page-scroll animate" href="index.php#skill">CANADA</a></li>

                                     <li><a class="page-scroll animate" href="index.php#skill">USA</a></li>

                                      <li><a class="page-scroll animate" href="index.php#skill">NEW ZEALAND</a></li>

                                            </ul>

                            </li>

                            <li><a class="page-scroll animate" href="#online">ONLINE ASSESSMENT</a></li>

                            <li><a class="page-scroll animate" href="#study">STUDY</a></li>

                            <li><a class="page-scroll animate" href="news_events.php">NEWS & EVENTS</a></li>

                            <li><a class="page-scroll animate" href="#contact">CONTACT US</a></li>



                       

                        </ul>

   <ul class="navbar-right social-network social-circle">

                           

  <li><a href="#" class="icoFacebook" title="Facebook"><i class="fa fa-facebook"></i></a></li>

                        <li><a href="#" class="icoTwitter" title="Twitter"><i class="fa fa-twitter"></i></a></li>

                        <li><a href="#" class="icoGoogle" title="Google +"><i class="fa fa-google-plus"></i></a></li>

                        <li><a href="#" class="icoLinkedin" title="Linkedin"><i class="fa fa-linkedin"></i></a></li>

                 

                           </ul>

                        </div>

                        <!--/.nav-collapse -->

                <!--   <a class="navbar-brand" href="index.html"><img src="img/eduaid.png" alt=""></a>   -->

                    </div>

                </nav>