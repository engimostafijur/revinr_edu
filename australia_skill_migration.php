<?php include 'header.php';?>
               
 <section id="skill_migraton" class="skill aust_business">
   <div class="container">
   <h4>Australia   &nbsp; &nbsp;| &nbsp; &nbsp; <cite title="Source Title"> Skilled Migration</cite></h4>


  <h3 class="fix">Business (Business Innovation and Investment) 
Migration</h3><br>

 <p>Business migration is an immense opportunity for 
many business personnel. As per the recent change, 
the migration process brings more flexibility and it 
is also considered as applicant friendly procedure. 
For this category there is no requirement for any 
specific academic degree or English language 
ability.</p>
 <p>Business migration is an immense opportunity for 
many business personnel. As per the recent change, 
the migration process brings more flexibility and it 
is also considered as applicant friendly procedure. 
For this category there is no requirement for any 
specific academic degree or English language 
ability.</p>
 
 </div>
 </section>
 <div class="container other_visa">
<h3>Engineers - Get CDR related help from Jane Hackett and the Team</h3>
<p>eduaid is proud to have Ms. Jane Hackett to oversee issues related to engineers from applying for the CDR approval 
to getting job in Australia. She has been working in the field since 1995. Ms. Hackett has substantial experience of 
reviewing and processing CDR and other related procedures for migrating to Australia. All engineering applications 
of eduaid are lodged after comprehensive review from Ms. Hackett and the team.</p><br><br>
</div>
 <div class="container" style="margin-bottom:40px;">
<div class="col-md-4">
<img src="img/pic7.jpg" class="img-responsive" alt="">

<div class="desc text-center">
            <p>General Professional</p>
				<a href="#"  class="btn btn-default sub1 sub2 btn-block">For More</a>
        </div>


</div>
<div class="col-md-4">
<img src="img/pic8.jpg" class="img-responsive" alt="">
<div class="desc text-center">
            <p>Engineers</p>
				<a href="#"  class="btn btn-default sub1 sub2 btn-block">For More</a>
        </div>
</div>
<div class="col-md-4">
<img src="img/pic9.jpg" class="img-responsive" alt="">
<div class="desc text-center">
            <p>ICT Professionals</p>
				<a href="#"  class="btn btn-default sub1 sub2 btn-block">For More</a>
        </div>
</div>
</div>
<?php include 'footer.php';?>