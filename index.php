<?php include 'header.php';?>
                 <section id="intro" class="intro-section">
                <div class="container-fluid demo-2">
                    <div id="slider" class="sl-slider-wrapper">

                        <div class="sl-slider">

                            <div class="sl-slide" data-orientation="horizontal" data-slice1-rotation="-25" data-slice2-rotation="-25" data-slice1-scale="2" data-slice2-scale="2">

                                <div class="sl-slide-inner">

                                    <div class="bg-img bg-img-1"></div>

                                    <div class="col-md-7">

                                    <h2>MIGRATION TO AUSTRALIA</h2>

                                   

                                    <blockquote><p> For better future</p></blockquote>

                              </div>

                                </div>

                            </div>

                            <div class="sl-slide" data-orientation="vertical" data-slice1-rotation="10" data-slice2-rotation="-15" data-slice1-scale="1.5" data-slice2-scale="1.5">

                                <div class="sl-slide-inner">

                                    <div class="bg-img bg-img-2"></div>

                                    <div class="col-md-7">

                                    <h2>MIGRATION TO AUSTRALIA

                                    </h2>

                                <blockquote><p> Engineers are in high demand</p></blockquote>

                            </div>

                            </div>

                        </div>

                        <div class="sl-slide" data-orientation="horizontal" data-slice1-rotation="3" data-slice2-rotation="3" data-slice1-scale="2" data-slice2-scale="1">

                            <div class="sl-slide-inner">

                                <div class="bg-img bg-img-3"></div>

                               <div class="col-md-7">

                                <h2>STUDY IN AUSTRALIA </h2>

  

                             <blockquote><p>With simpler, faster visas</p></blockquote>

                        </div>

                        </div>

                    </div>

                        <div class="sl-slide" data-orientation="horizontal" data-slice1-rotation="3" data-slice2-rotation="3" data-slice1-scale="2" data-slice2-scale="1">

                            <div class="sl-slide-inner">

                                <div class="bg-img bg-img-4"></div>

                                <div class="col-md-7"><h2>BUSINESS MIGRATION TO AUSTRALIA</h2>

                             <blockquote><p>Until he extends the circle of his compassion to all living things, man will not himself find peace.</p></blockquote>

                        </div>

                        </div>

                    </div>

            </div> 

          <nav id="nav-dots" class="nav-dots">

                <span class="nav-dot-current"></span>

                <span></span>

                <span></span>

                <span></span>

            </nav>

            </div>
        </div>

  </section>

  <!-- About Section -->

    <section id="about" class="about-section">

        <div class="container-fluid">

            <div class="container">

                <h2>ABOUT <span>US</span></h2>

                <p>eduaid is a leading Australian Registered Immigration Law firm, which is recognized by Australia and New Zealand Government's immigrati-

                on authorities. eduaid was established in 2000 and since then it has been cooperating professionals and students to achieve their goals. </p>

                <p>The head office of eduaid is located in Sydney, Australia. Its branch offices are located in the central business districts of different countries

                in Asia, Middle-East and North America. </p>

                <p>eduaid attributes its success to people who constitute eduaid and the ever-growing client base largely because of the referrals and strict

                adherence to ethical policies. It provides step-by-step guidance to its entire client base not through standard steps rather defining each

                clients' individual requirements with pre and post departure arrangements all under one roof.</p><br><br><br><br>

                <ul class="list-inline text-center">

                    <li><img src="img/logo1.png" class="img-responsive" alt=""></li>

                    <li><img src="img/logo2.png" class="img-responsive" alt=""></li>

                    <li><img src="img/logo3.png" class="img-responsive" alt=""></li>

                    <li><img src="img/logo4.png" class="img-responsive" alt=""></li>

                    <li><img src="img/logo5.png" class="img-responsive"  alt=""></li>

                </ul>

            </div>

        </div>

      </section>

        <!-- skill Section -->

  <section id="skill" class="skill parallax fixed" style="background-image: url(img/skill_bg.jpg);">

        <div class="container-fluid">

            <div class="container">

                <div class="row">

                    <div class="col-md-6">

                        <h2>VISA TYPES</h2>

                        <p>Australia is considered as most migrant friendly country in the

                        world, with pure combination of cultures and heritages from

                        all around the world. According to a survey by 'The Economists',

                        Melbourne, Australia is ranked as the most livable city in the world.

                        Three other Australian cities (Sydney, Perth and Adelaide) claimed

                        positions in the top ten as well. </p>

                    </div>

                    <div class="col-md-6">

                        <div class="row">
                            <div class="col-md-6 skill_mig">

                                <h4>Australia</h4>

                                <p>Australian Migration - The Demand

                                Driven and Systematic Approach</p>

                                <p>Australian migration system is consi

                                dered as most demand driven and

                                ap....</p>

                                <div class="box_text"></div>

                                <div class="icon">

                                    <!-- <i class="fa fa-plus"></i> -->

                                   <a href="aust_inner.php"> <img src="img/plus2.png"  alt=""></a>
                                </div>

                            </div>

                            <div class="col-md-6 skill_mig">

                                <h4>Canada</h4>

                              <p>Australian Migration - The Demand

                                Driven and Systematic Approach</p>

                                <p>Australian migration system is consi

                                dered as most demand driven and

                                ap....</p>

                                <div class="box_text"></div>

                                <div class="icon">

                                   <a href="#"> <img src="img/plus2.png"  alt=""></a>
                                </div>

                            </div>

                        </div>

                        <div class="row">

                            <div class="col-md-6 skill_mig">

                                <h4>USA</h4>

                                  <p>Australian Migration - The Demand

                                Driven and Systematic Approach</p>

                                <p>Australian migration system is consi

                                dered as most demand driven and

                                ap....</p>

                                <div class="box_text"></div>

                                <div class="icon">

                                   <a href="australia_business_migration.php"> <img src="img/plus2.png"  alt=""></a>

                                </div>

                            </div>

                            <div class="col-md-6 skill_mig">

                                <h4>New Zealand</h4>

                                 <p>Australian Migration - The Demand

                                Driven and Systematic Approach</p>

                                <p>Australian migration system is consi

                                dered as most demand driven and

                                ap....</p>

                                <div class="box_text"></div>

                                <div class="icon">

                                   <a href="australia_others_visa.php"> <img src="img/plus2.png"  alt=""></a>

                                </div>

                            </div>



                        </div>

                    </div>

                </div>

            </div>

        </div>

    </section>
<section id="online" class="online-section">
<div class="container-fluid">

<div class="container">

  <div class="row">

    <div class="col-md-6">

      <h2>ONLINE </h2>

      <h3>ASSESSMENT</h3>

      <p>Instant Skilled Visa Eligibility Assessment - Australia</p><br><br><br>

      <img src="img/pic1.png" class="img-responsive" alt=""><br>
      <div class="col-md-10">  <p>Instant Skilled Visa Eligibility Assessment - Australia Instant Skilled Visa Eligibility Assessment - Australia Instant Skilled Visa Eligibility Assessment - Australia Instant Skilled Visa Eligibility Assessment </p>

 </div>

</div> 
 <div class="col-md-6">

    <div class="row">

     <ul class="nav nav-pills aust_form pull-right">

        <li class="active"><a data-toggle="pill" href="#aust">Australia</a></li>

        <li><a data-toggle="pill" href="#menu1">Canada</a></li>    

  </ul> 

  </div>
  <div class="tab-content">

    <div id="aust" class="tab-pane fade in active">
<form class="form-horizontal" role="form">

<div class="form-group">

    <label for="name" class="col-sm-6 control-label">Your Country of Citizenship</label>        

    <select class="selectpicker col-xs-12 col-sm-6">

    <option>-- Select Country --</option>

    <option>Ketchup</option>

    <option>Relish</option>

    </select>       

</div>
<div class="form-group">

        <label for="name" class="col-sm-6 control-label">Your Name*</label>

        <div class="col-sm-6">

        <input type="text" class="form-control" id="name" name="name"  required>

        </div>

    </div>

  <div class="form-group dob">

    <label for="name" class="col-xs-12 col-sm-6 control-label">Date of Birth</label>        

    <select class="selectpicker col-xs-6 col-sm-2">

    <option>Day</option>

    <option>01</option>

    <option>02</option>

     <option>03</option>

    </select> 

     <select class="selectpicker col-xs-6 col-sm-2">

    <option>Month</option>

    <option>January</option>

    <option>February</option>

     <option>March</option>

    </select> 

     <select class="selectpicker col-xs-6 col-sm-2">

    <option>Year</option>

    <option>2000</option>

    <option>2001</option>

     <option>2002</option>

    </select>       

</div>

    <div class="form-group">

      <label class="control-label col-sm-6" for="email">E-mail Address*</label>

      <div class="col-sm-6">

        <input type="email" class="form-control" id="email" required>

      </div>

    </div>

      <div class="form-group">

      <label class="control-label col-sm-6" for="mobile">Contact Number*</label>

      <div class="col-sm-6">
     <input type="text" class="form-control" id="mobile" name="mobile"  required>

      </div>

    </div>

<div class="form-group">

    <label for="name" class="col-sm-6 control-label">Age Range</label>        

    <select class="selectpicker col-xs-12 col-sm-6">

    <option>18 - 24</option>

    <option>Ketchup</option>

    <option>Relish</option>

    </select>       

</div>
    <div class="form-group">

    <label for="name" class="col-sm-6 control-label">Your Current Occupation</label>        

    <select class="selectpicker col-xs-12 col-sm-6">

    <option>-- Please Select --</option>

    <option>Ketchup</option>

    <option>Relish</option>

    </select>       

</div>   

    <div class="form-group">

      <label class="control-label col-sm-6" for="resume">Attach Resume (optional but recommended)</label>

      <div class="col-sm-6 input05">          

       <input type="file" id="input05"> 

      </div>

    </div>



  <div class="form-group">

    <label for="name" class="col-sm-6 control-label">English Language Proficiency</label>        

    <select class="selectpicker col-xs-12 col-sm-6">

    <option>-- Competent English --</option>

    <option>Ketchup</option>

    <option>Relish</option>

    </select>       

</div>

<div class="form-group">

    <label for="name" class="col-sm-6 control-label">Overseas Skilled Employment in

Nominated Occupation</label>        

    <select class="selectpicker col-xs-12 col-sm-6">

    <option>-- Less Than 3 Years --</option>

    <option>Ketchup</option>

    <option>Relish</option>

    </select>       

</div>

<div class="form-group">

    <label for="name" class="col-sm-6 control-label">Your Highest Academic Qualification Achieved</label>        

    <select class="selectpicker col-xs-12 col-sm-6">

    <option>-- Diploma or Trade --</option>

    <option>Ketchup</option>

    <option>Relish</option>

    </select>       

</div>

         

    <div class="form-group">

    <label for="name" class="col-sm-6 control-label">Have you ever been to Australia? </label>        

    <select class="selectpicker col-xs-12 col-sm-6">

    <option>-- Please Select --</option>

    <option>Yes</option>

    <option>No</option>

    </select>       

</div>



    <div class="form-group">    

     <label class="col-sm-6"> </label>            

      <div class="col-sm-6 text-center">

        <button type="submit" class="btn btn-default sub">Submit</button>

      </div>

    </div>

  </form>

    </div>

    <div id="menu1" class="tab-pane fade">

   <form class="form-horizontal" role="form">

<div class="form-group">

    <label for="name" class="col-sm-6 control-label">Your Country of Citizenship</label>        

    <select class="selectpicker col-xs-12 col-sm-6">

    <option>-- Select Country --</option>

    <option>Ketchup</option>

    <option>Relish</option>

    </select>       

</div>
<div class="form-group">

        <label for="name" class="col-sm-6 control-label">Your Name*</label>

        <div class="col-sm-6">

            <input type="text" class="form-control" id="name" name="name"  required>

        </div>

    </div>

  <div class="form-group dob">

    <label for="name" class="col-xs-12 col-sm-6 control-label">Date of Birth</label>        

    <select class="selectpicker col-sm-2">

    <option>Day</option>

    <option>01</option>

    <option>02</option>

     <option>03</option>

    </select> 

     <select class="selectpicker col-sm-2">

    <option>Month</option>

    <option>January</option>

    <option>February</option>

     <option>March</option>

    </select> 

     <select class="selectpicker col-sm-2">

    <option>Year</option>

    <option>2000</option>

    <option>2001</option>

     <option>2002</option>

    </select>       

</div>
    <div class="form-group">

      <label class="control-label col-sm-6" for="email">E-mail Address*</label>

      <div class="col-sm-6">

        <input type="email" class="form-control" id="email" required>

      </div>

    </div>
      <div class="form-group">

      <label class="control-label col-sm-6" for=m"obile">Contact Number*</label>

      <div class="col-sm-6">
     <input type="text" class="form-control" id="mobile" name="mobile"  required>

      </div>

    </div>

<div class="form-group">

    <label for="name" class="col-sm-6 control-label">Age Range</label>        

    <select class="selectpicker col-xs-12 col-sm-6">

    <option>18 - 24</option>

    <option>Ketchup</option>

    <option>Relish</option>

    </select>       

</div>
    <div class="form-group">

    <label for="name" class="col-sm-6 control-label">Your Current Occupation</label>        

    <select class="selectpicker col-xs-12 col-sm-6">

    <option>-- Please Select --</option>

    <option>Ketchup</option>

    <option>Relish</option>

    </select>       

</div>

    <div class="form-group">

  <label for="name" class="col-sm-6 control-label">Attach Resume (optional but recommended)</label>

  <div class="col-sm-6 input05">         

        <input type="file" id="input06">

      </div>

</div> 

  <div class="form-group">

    <label for="name" class="col-sm-6 control-label">English Language Proficiency</label>        

    <select class="selectpicker col-xs-12 col-sm-6">

    <option>-- Competent English --</option>

    <option>Ketchup</option>

    <option>Relish</option>

    </select>       

</div>

<div class="form-group">

    <label for="name" class="col-sm-6 control-label">Overseas Skilled Employment in

Nominated Occupation</label>        

    <select class="selectpicker col-xs-12 col-sm-6">

    <option>-- Less Than 3 Years --</option>

    <option>Ketchup</option>

    <option>Relish</option>

    </select>       

</div>

<div class="form-group">

    <label for="name" class="col-sm-6 control-label">Your Highest Academic Qualification Achieved</label>        

    <select class="selectpicker col-xs-12 col-sm-6">

    <option>-- Diploma or Trade --</option>

    <option>Ketchup</option>

    <option>Relish</option>

    </select>       

</div>
    <div class="form-group">

    <label for="name" class="col-sm-6 control-label">Have you ever been to Australia? </label>        

    <select class="selectpicker col-sm-6">

    <option>-- Please Select --</option>

    <option>Yes</option>

    <option>No</option>

    </select>       

</div>
    <div class="form-group">    

     <label class="col-sm-6"> </label>            

      <div class="col-sm-6 text-center">

        <button type="submit" class="btn btn-default sub">Submit</button>

      </div>

    </div>

  </form>
    </div>
  </div>

</div>
      </div>

  </div>

</div>

 </section>

 <section id="study" class="study-section">

<div class="container-fluid">

  <div class="row">

    <div class="cover-card col-sm-3" style="background: url(img/pic2.jpg) no-repeat center top;background-size:cover;">

      <h2>STUDY</h2>

      <p>Explore The Opportunities</p>

    </div>

    <div class="cover-card col-sm-3" style="background: url(img/pic3.jpg) no-repeat center top;background-size:cover;"></div>    <div class="cover-card col-sm-3" style="background: url(img/pic4.jpg) no-repeat center top;background-size:cover;">

  

    </div>

    <div class="cover-card col-sm-3" style="background: url(img/pic5.jpg) no-repeat center top;background-size:cover;">

    

    </div>



  </div>

</div>



<div class="container">

  <h2 class="text-center">FIND UNIVERSITY</h2>





  <form class="form-horizontal" role="form" id="msform">

  <fieldset>

    

   

  

<div class="form-group origin">

    <label for="name" class="col-sm-4 control-label">Country of Origin</label>        

    <select class="selectpicker col-xs-12 col-sm-8">

    <option>-- Select Country --</option>

    <option>Bangladesh</option>

    <option>India</option>

    </select>       

</div>

         

    <div class="form-group origin">

    <label for="name" class="col-sm-4 control-label">Preferred Country</label>        

    <select class="selectpicker col-xs-12 col-sm-8">

    <option>-- Select Country --</option>

    <option>Bangladesh</option>

    <option>India</option>

    </select>       

</div>

<div class="form-group">

    <label for="name" class="col-sm-4 control-label">Program Type</label>        

    <select class="selectpicker col-xs-12 col-sm-8">

   

    <option>Diploma</option>

    <option>Bachelor's</option>

     <option>Master's</option>

    </select>       

</div>

         

    <div class="form-group">

    <label for="name" class="col-sm-4 control-label">Course Group</label>        

    <select class="selectpicker col-xs-12 col-sm-8">

   

    <option>Accounting, Finance</option>

    <option>Agriculture</option>

    <option>Architecture and Design</option>

     <option>Arts</option>

      <option>Aviation, Aeronautical, Aerospace</option>

       <option>Bio-Chemistry</option>

    </select>       

</div>



<div class="form-group">

    <label for="name" class="col-sm-4 control-label">IELTS (optional)</label> 

    <div class="col-sm-8">       

     <input type="text" class="form-control" name="IELTS" placeholder="IELTS score">

     </div>

</div>

  <input type="button" name="next" class="next action-button pull-right" value="Next" />

  </fieldset>

    <fieldset>
    <div class="form-group">

    <label for="name" class="col-sm-4 control-label">TOEFL TYPE</label> 

    <select class="selectpicker col-sm-8">
    <option>CBT</option>

    <option>IBT</option>

    <option>PBT</option>
    </select> 
</div>

  <div class="form-group">

    <label for="name" class="col-sm-4 control-label">TOEFL (optional)</label> 

    <div class="col-sm-8">       

       <input type="text" class="form-control" name="TOEFL" placeholder="TOEFL score"> 

       </div> 

</div>

  <div class="form-group">

    <label for="name" class="col-sm-4 control-label">SAT (optional)</label> 

    <div class="col-sm-8">       

       <input type="text" class="form-control" name="SAT" placeholder="SAT score"> 

       </div> 

</div>

  <div class="form-group">

    <label for="name" class="col-sm-4 control-label">GMAT (optional)</label> 

    <div class="col-sm-8">       

       <input type="text" class="form-control" name="GMAT" placeholder="GMAT score"> 

       </div> 

</div>

  <div class="form-group">

    <label for="name" class="col-sm-4 control-label">GRE (optional)</label> 

    <div class="col-sm-8">       

       <input type="text" class="form-control" name="GRE" placeholder="GRE score"> 

       </div> 

</div>



    <input type="submit" name="submit" class="submit action-button pull-right" value="Submit" />

      <input type="button" name="previous" class="previous action-button pull-right" value="Previous" />

  </fieldset>
  </form>
</div>
 </section>
  <section id="carousel"> 
<div class="container">

 <h2 class="text-center">TESTIMONIALS</h2>

<div class="row">

                    <div class="col-md-12" data-wow-delay="0.2s">

                        <div class="carousel slide" data-ride="carousel" id="quote-carousel">

                            <!-- Bottom Carousel Indicators -->

                            <ol class="carousel-indicators">

                              <li data-target="#quote-carousel" data-slide-to="0" class="active"><img class="img-responsive " src="img/tanvir.png" alt="" >   </li>

                                <li data-target="#quote-carousel" data-slide-to="1"><img class="img-responsive" src="img/tanvir.png" alt=""> </li>

                                <li data-target="#quote-carousel" data-slide-to="2"><img class="img-responsive" src="img/tanvir.png" alt=""> </li>

                                <li data-target="#quote-carousel" data-slide-to="3"><img class="img-responsive" src="img/tanvir.png" alt=""> </li>

                                <li data-target="#quote-carousel" data-slide-to="4"><img class="img-responsive" src="img/tanvir.png" alt=""> </li>

                                <li data-target="#quote-carousel" data-slide-to="5"><img class="img-responsive" src="img/tanvir.png" alt="">  </li>

                            

                               </ol>



                            <!-- Carousel Slides / Quotes -->

                            <div class="carousel-inner text-center">



                                <!-- Quote 1 -->

                                <div class="item active">

                                    <blockquote>

                                        <div class="row">

                                            <div class="col-sm-8 col-sm-offset-2">



                                                <p>Eduaid has been very helpful to the applicants who intended to migrate Australia. We received legal advice and support from the friendly staffs of Eduaid whenever needed in the complicated visa processing. Thanks, and well wishes.</p>

                                              <small>Dr. Russell Tanvir Chowdhury <br>Senior Medical Officer <br>Blacktown Hospital, Sydney, NSW, Australia</small>

                                                

                                            </div>

                                        </div>

                                    </blockquote>

                                </div>

                                <!-- Quote 2 -->

                                <div class="item">

                                    <blockquote>

                                        <div class="row">

                                            <div class="col-sm-8 col-sm-offset-2">



                                                <p>Eduaid has been very helpful to the applicants who intended to migrate Australia. We received legal advice and support from the friendly staffs of Eduaid whenever needed in the complicated visa processing. Thanks, and well wishes.</p>

                                             <small>Dr. Russell Tanvir Chowdhury <br>Senior Medical Officer <br>Blacktown Hospital, Sydney, NSW, Australia</small>

                                                

                                            </div>

                                        </div>

                                    </blockquote>

                                </div>

                                <!-- Quote 3 -->

                                <div class="item">

                                    <blockquote>

                                        <div class="row">

                                            <div class="col-sm-8 col-sm-offset-2">



                                                <p>Eduaid has been very helpful to the applicants who intended to migrate Australia. We received legal advice and support from the friendly staffs of Eduaid whenever needed in the complicated visa processing. Thanks, and well wishes.</p>

                                                 <small>Dr. Russell Tanvir Chowdhury <br>Senior Medical Officer <br>Blacktown Hospital, Sydney, NSW, Australia</small>

                                                

                                            </div>

                                        </div>

                                    </blockquote>

                                </div>

                                  <!-- Quote 4 -->

                                <div class="item">

                                    <blockquote>

                                        <div class="row">

                                            <div class="col-sm-8 col-sm-offset-2">



                                                <p>Eduaid has been very helpful to the applicants who intended to migrate Australia. We received legal advice and support from the friendly staffs of Eduaid whenever needed in the complicated visa processing. Thanks, and well wishes.</p>

                                                <small>Dr. Russell Tanvir Chowdhury <br>Senior Medical Officer <br>Blacktown Hospital, Sydney, NSW, Australia</small>

                                                



                                            </div>

                                        </div>

                                    </blockquote>

                                </div>

                                  <!-- Quote 5 -->

                                <div class="item">

                                    <blockquote>

                                        <div class="row">

                                            <div class="col-sm-8 col-sm-offset-2">



                                                 <p>Eduaid has been very helpful to the applicants who intended to migrate Australia. We received legal advice and support from the friendly staffs of Eduaid whenever needed in the complicated visa processing. Thanks, and well wishes.</p>

                                                <small>Dr. Russell Tanvir Chowdhury <br>Senior Medical Officer <br>Blacktown Hospital, Sydney, NSW, Australia</small>

                                                

                                            </div>

                                        </div>

                                    </blockquote>

                                </div>

                                  <!-- Quote 6 -->

                                <div class="item">

                                    <blockquote>

                                        <div class="row">

                                            <div class="col-sm-8 col-sm-offset-2">



                                                 <p>Eduaid has been very helpful to the applicants who intended to migrate Australia. We received legal advice and support from the friendly staffs of Eduaid whenever needed in the complicated visa processing. Thanks, and well wishes.</p>

                                                <small>Dr. Russell Tanvir Chowdhury <br>Senior Medical Officer <br>Blacktown Hospital, Sydney, NSW, Australia</small>

                                                

                                            </div>

                                        </div>

                                    </blockquote>

                                </div>





                            </div>

                    </div>

                  

                    <br> <br>

                        <p class="col-md-2 col-xs-offset-5"><a href="#" class="btn btn-default sub1" role="button">View All Testimonial</a></p>

                    </div>

                </div>

    </div>
  </section>
<?php include 'footer.php';?>