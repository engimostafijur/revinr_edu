<?php include 'header.php';?>
               

			<div class="container news">  

			<div class="eve">
			<h2>NEWS & <span>EVENTS</span></h2>
			<p>Migration & visa related news</p>
		</div>
		<div class="row">
	<div class="col-md-9 news_date">
<div class="row">
	<small>October 08, 2015</small>
<h3 style="margin-top:0;">Australia - Boost for $1bn international student market</h3>
<div class="col-md-4">
<img src="img/pic10.jpg" alt="" class="img-responsive" >
</div>
<div class="col-md-8">
<hr>
<p>Professor Stirling, who chairs of the South Australian Vice-Chancellors’ Committee, said: “More int
ernational students than ever before are recognising the benefits that Adelaide and South Australi
a have to offer as an education destination. </p>
<p>“Each year, South Australia welcomes international students from more than 125 countries, and 
we want to ensure that potential students everywhere are aware of the highly regarded study oppo
rtunities available to them. </p>

<p>“We offer world-class degrees in high quality education facilities in a modern, accessible, affordab
le and safe location.”</p>
<p>In 2014, South Australia hosted more than 30,000 international students from about 125 countries 
and international education services accounted for $1.056 billion in export earnings, which was 
the largest service export for SA, and the sixth largest export overall. </p>
<p>Professor Stirling, who chairs of the South Australian Vice-Chancellors’ Committee, said: “More int
ernational students than ever before are recognising the benefits that Adelaide and South Australi
a have to offer as an education destination. </p>

<p> “We offer world-class degrees in high quality education facilities in a modern, accessible, affordab
le and safe location.”</p><br><br><br>
</div>
</div>
</div>


			<div class="col-md-3 news_right">

			<h3>More News</h3>
	
<div>
<div class="row">
<div class="col-md-4">
<img src="img/pic11.jpg" alt="" class="img-responsive" >
</div>	
<div class="col-md-8">
<small>Sep 20, 2015</small>
<p>Your Career
Starts Here</p>
</div>	
</div>	
<hr>
<div class="row">
<div class="col-md-4">
<img src="img/pic11.jpg" alt="" class="img-responsive" >
</div>	
<div class="col-md-8">
<small>Sep 20, 2015</small>
<p>Your Career
Starts Here</p>
</div>	
</div>	
<hr>
<div class="row">
<div class="col-md-4">
<img src="img/pic11.jpg" alt="" class="img-responsive" >
</div>	
<div class="col-md-8">
<small>Sep 20, 2015</small>
<p>Your Career
Starts Here</p>
</div>	
</div>	

</div>			
			
			</div>
			</div>
				</div>

<?php include 'footer.php';?>