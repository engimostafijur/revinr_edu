<?php include 'header.php';?>

        <!-- skill Section -->
  <section id="skill" class="skill parallax fixed" style="background-image: url(img/skill_bg.jpg);">
        <div class="container-fluid">
            <div class="container">
                <div class="row">
                
                        <h2>AUSTRALIA </h2>
                        <h3>MIGRATION</h3>
                        <p>Australia is considered as most migrant friendly country in the
                        world, with pure combination of cultures and heritages from
                        all around the world. According to a survey by 'The Economists',
                        Melbourne, Australia is ranked as the most livable city in the world.
                        Three other Australian cities (Sydney, Perth and Adelaide) claimed
                        positions in the top ten as well. </p>
                  
              
                </div>
            </div>
        </div>

  <div class="container" id="tourpackages-carousel">
      
      <div class="row">
        
        <div class="col-xs-18 col-sm-6 col-md-3">
          <div class="thumbnail">
          <img src="img/skilled.jpg"  alt="">
              <div class="caption">
                <h4>Skilled Migration Visa</h4>
                <p>Australian Migration - The Demand Driven and Systematic Approach Australian migration system is consi dered as most demand driven and ap....</p>

                <p><a class="btn btn-default" href="australia_skill_migration.php" role="button">DETAILS <i class="fa fa-angle-right"></i></a></p>
            </div>
          </div>
        </div>

        <div class="col-xs-18 col-sm-6 col-md-3">
          <div class="thumbnail">
            <img src="img/business.jpg"  alt="">
              <div class="caption">
                <h4>Business Migration Visa</h4>
                 <p>Australian Migration - The Demand Driven and Systematic Approach Australian migration system is consi dered as most demand driven and ap....</p>
 <p><a class="btn btn-default" href="australia_business_migration.php" role="button">DETAILS <i class="fa fa-angle-right"></i></a></p>
            </div>
          </div>
        </div>

        <div class="col-xs-18 col-sm-6 col-md-3">
          <div class="thumbnail">
           <img src="img/student.jpg"  alt="">
              <div class="caption">
                <h4>Student Visa</h4>
                 <p>Australian Migration - The Demand Driven and Systematic Approach Australian migration system is consi dered as most demand driven and ap....</p>
 <p><a class="btn btn-default" href="#" role="button">DETAILS <i class="fa fa-angle-right"></i></a></p>
            </div>
          </div>
        </div>

        <div class="col-xs-18 col-sm-6 col-md-3">
          <div class="thumbnail">
            <img src="img/pic12.jpg"  alt="">
              <div class="caption">
                <h4>Other Visa Types</h4>
                <p>Australian Migration - The Demand Driven and Systematic Approach Australian migration system is consi dered as most demand driven and ap....</p>
 <p><a class="btn btn-default" href="australia_others_visa.php" role="button">DETAILS <i class="fa fa-angle-right"></i></a></p>
            </div>
          </div>
        </div>
        
      </div><!-- End row -->
      
    </div><!-- End container -->
    </section>



<?php include 'footer.php';?>