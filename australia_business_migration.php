<?php include 'header.php';?>
      <section id="skill" class="aust_business">
   <div class="container">
   <h4>Australia   &nbsp; &nbsp;| &nbsp; &nbsp; <cite title="Source Title"> Business Migration</cite></h4>
 <div class="col-md-4"> 
 <blockquote>
  <p>Business (Business Innovation and Investment) 
Migration</p>
</blockquote>
 <p>Business migration is an immense opportunity for 
many business personnel. As per the recent change, 
the migration process brings more flexibility and it 
is also considered as applicant friendly procedure. 
For this category there is no requirement for any 
specific academic degree or English language 
ability.</p>
 </div>
 <div class="col-md-4 basic"> 
  <blockquote>
  <p>Business Migration - Basic Requirements</p>
</blockquote>
 <p>Having substantial business experience</p>

 <p>Having min. 500,000 AUD yearly business turnover</p>

 <p>Having min. 800,000 AUD of total asset Must fulfill </p>
the basic point requirement set by DIBP </p>

 <p>For reviewing your business migration prospect, 
please email us your details on: info@eduaid.net </p>
 </div>
  <div class="col-md-4"> 
   <blockquote>
  <p>Precaution</p>
</blockquote>
  <p>Buying property in Australia does not guarantee 
or fulfill requirement to migrate to Australia.</p>
 </div>
 </div>
 </section>            

<?php include 'footer.php';?>