<?php include 'header.php';?>
               
 <section id="faq">
 
<div class="container">
<h2><span>SERVICES</span></h2>
<div class="col-md-8">
	<h4>SEVIS and TOEFL/GRE/GMAT Registration Fee</h4>
<p>Worried about the process of paying the SEVIS and TOEFL/GRE/GMAT registration fee? We will help you 
out and there is no service charge involved. </p>
<h4>Our promise to the students</h4>
<p>eduaid assist students to make an informed decision on what is best for them. Counselors of eduaid can 
provide most suited ideas and information on international education. eduaid agrees to provide the 
following services to international students and prospective international students as required:</p>

<ul class="list-unstyled impartial">
<li>Impartial counseling to students for their future study options. Academic Counseling & Assistance in 
course selection. Possible study pathways - showing students in various ways that they can reach 
their study goals. </li>
<li>Arranging and assisting students for the IELTS preparation course. </li>
<li>Providing detailed relevant information on costs, program structures, course outcomes, etc to allow students 
to compare programs and decide which suites them best. </li>
<li>Informing students about the relevant Student Visa and other issues. </li>
<li>Guiding on making applications for study at an International institution. Assist them to prepare documents for 
their applications for maximum chance of a successful outcome. Other ancillary services relevant to a student's 
application</li>
<li>Guiding and assisting students for their Visa applications lodgement. </li>
<li>On-going support to students and their parents after an application for study has been submitted. </li>
<li>Assist on getting Letter of Offer/I-20/Electronic Confirmation of Enrollment from the institution. </li>
</ul>
</div>
</div>
 </section>
<?php include 'footer.php';?>